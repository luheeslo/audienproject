# -*- coding: utf-8 -*-
import sys

reload(sys)
sys.setdefaultencoding('utf-8')

from django import forms
from audien.models import Instituicao, PeriodoLetivo, UserProfile
from django.core.exceptions import ValidationError

class InstForm(forms.ModelForm):

    class Meta:
        model = Instituicao
        fields = ['nome', 'sigla', 'fone']


class PerLetForm(forms.ModelForm):
 
    """def is_valid(self):

        valid = super(PerLetForm, self).is_valid()

        if not valid:
            return valid

        try:
            inst = self.cleaned_data['instituicao']
            periodo_letivo = inst.periodoletivo_set.get(ano=self.cleaned_data['ano'],
                                                        periodo=self.cleaned_data['periodo'])

            self._errors['instituicao'] = u'Periodo Letivo já cadastrado nessa instituição'
            print self._errors
            return False
        except PeriodoLetivo.DoesNotExist:
            return True"""


    """def clean(self):
            try: 
                inst = self.cleaned_data.get('instituicao')
                periodo_letivo = inst.periodoletivo_set.get(ano=self.cleaned_data['ano'], 
                                                        periodo=self.cleaned_data['periodo'])
                raise ValidationError('Periodo Letivo já existe no(a) %s' % (inst.sigla))
            except PeriodoLetivo.DoesNotExist:
                return self.cleaned_data"""
	    
    def clean_instituicao(self):

	inst = self.cleaned_data.get('instituicao')
	
	try: 
	    periodo_letivo = inst.periodoletivo_set.get(ano=self.cleaned_data['ano'], 
						    periodo=self.cleaned_data['periodo'])
	    raise ValidationError('Periodo Letivo já existe nessa instituição')
	except PeriodoLetivo.DoesNotExist:
	    return inst 
    
    class Meta:
        model = PeriodoLetivo


class PerLetUpdateForm(forms.ModelForm):

    class Meta:
	model = PeriodoLetivo
	fields = ['ano', 'periodo', 'inicio', 'fim']
