# -*- coding: utf-8 -*-
from django.db import models
from datetime import datetime
from django.core.validators import RegexValidator	
from django.contrib.auth.models import User
class Instituicao(models.Model):
	
	nome = models.CharField(max_length=80, unique=True, verbose_name='Nome')
	sigla = models.CharField(max_length=10, verbose_name='Sigla')
	fone = models.CharField(max_length=13, 
                                verbose_name='Fone', 
                                validators=[RegexValidator(r'\([1-9][1-9]\)[2-9][0-9]{3}-[0-9]{4}', 
                                                          "O formato é (XX)XXXX-XXXX")])	
	periodo_atual = models.OneToOneField('PeriodoLetivo', editable=False,null=True, related_name='perlet_atual',on_delete=models.SET_NULL)
	
	def mudarPeriodoAtual(self, pk):
		perlet = self.periodoletivo_set.get(pk=pk)
		perlet.save()
		self.save()


	def __unicode__(self):
		return self.sigla

	def save(self, *args, **kwargs):
		print 'Passa por aqui'
                if len(self.periodoletivo_set.all()) > 0:
                        print 'por aqui tbm!'
			self.periodo_atual = self.periodoletivo_set.all()[0]	
		super(Instituicao, self).save(*args, **kwargs)
	
	class Meta:
		ordering = ['-nome']
		verbose_name = 'Instituição'
		verbose_name_plural = 'Instituições'


class PeriodoLetivo(models.Model):
	
	PERIODOS = (
			(1, '1'), 
		   	(2, '2')
		   )
	
	ano = models.PositiveSmallIntegerField()
	periodo = models.PositiveSmallIntegerField(choices=PERIODOS, default=1)	
	inicio = models.DateField()
	fim = models.DateField()	
	instituicao = models.ForeignKey(Instituicao, verbose_name='Instituição')
	ultima_atualizacao = models.DateTimeField(editable=False, null=True, auto_now=True)	


	def __unicode__(self):
		return '%d.%d' % (self.ano, self.periodo) 

	class Meta:		
		
		ordering = ['-ultima_atualizacao']
		verbose_name = 'Período letivo'
		verbose_name_plural = 'Períodos Letivos'	
	
class Estudante(models.Model):
        
        nome = models.CharField(max_length=100, verbose_name='Nome')
        matricula = models.CharField(max_length=8, validators=[RegexValidator(r'\d{8}', 'Máximo de  8 números')],
                                     verbose_name='Matrícula', 
                                     unique=True)
        instituicaoAtual = models.ForeignKey(Instituicao, null=True, blank=True, 
                                            on_delete=models.SET_NULL, 
                                            verbose_name='Instituição Atual')

        def __unicode__(self):
            return self.nome
        
        class Meta:
                ordering = ['nome']
                verbose_name = 'Estudante'
                verbose_name_plural = 'Estudantes'

class Declaracao(models.Model):

    dataRecebimento = models.DateField(verbose_name='Data de Recebimento')
    observacao = models.CharField(max_length=100, verbose_name='Observação')
    estudante = models.ForeignKey(Estudante, verbose_name='Estudante')
    periodo_letivo = models.ForeignKey(PeriodoLetivo, verbose_name="Periodo Letivo")

    class Meta:
        ordering = ['-dataRecebimento']
        verbose_name = 'Declaração'
        verbose_name_plural = 'Declarações'

class UserProfile(models.Model):

    user = models.OneToOneField(User)

    email = models.EmailField()
    
    def __unicode__(self):
        return self.user.username
