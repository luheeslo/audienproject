from django.conf.urls import patterns, include, url
from audien import views
from audien import models
from audien.forms import PerLetForm
from django.core.urlresolvers import reverse
from django.views.generic import TemplateView
from django.views.generic.edit import CreateView
from django.views.generic.list import ListView
from django.views.generic.edit import DeleteView 
from django.views.generic.edit import UpdateView 
from audien.forms import PerLetUpdateForm

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'projetoRAD.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
        
    url(r'^login/$','django.contrib.auth.views.login', name='login'),

    url(r'^logout/$','django.contrib.auth.views.logout',{'next_page': '/audien/login/'} ,name='logout'),

    url(r'^$', TemplateView.as_view(template_name='audien/index.html'), name='index'),
    
    url(r'^create_inst/$', CreateView.as_view(model=models.Instituicao, 
                        success_url='/audien/list_inst/'), name='create_inst'),
    
    url(r'^list_inst/$', ListView.as_view(model=models.Instituicao, paginate_by=3), name='list_inst'),
    
    url(r'^inst/(?P<pk>\d+)$', UpdateView.as_view(model=models.Instituicao,
                                                  template_name='audien/instituicao_update.html',
                                                  success_url='/audien/list_inst/'
                                                 ), name='update_inst'),

    url(r'^del_inst/(?P<pk>\d+)$', DeleteView.as_view(model=models.Instituicao,
                                                      success_url='/audien/list_inst/'),
                                   name='delete_inst'),

    url(r'^create_perlet/$', views.PerLetCreateView.as_view(model=models.PeriodoLetivo,
                                                form_class=PerLetForm,
                                                success_url='/audien/'), name='create_perlet'),


    url(r'^list_perlet/(?P<pk>\d+)$', views.PerLetView.as_view(model=models.PeriodoLetivo, paginate_by=5), name='list_perlet'),

    
    url(r'^del_perlet/(?P<pk>\d+)$', views.PerLetDeleteView.as_view(model=models.PeriodoLetivo), name='delete_perlet'),


    url(r'^perlet/(?P<pk>\d+)$', views.PerLetUpdateView.as_view(model=models.PeriodoLetivo,
                                                    form_class=PerLetUpdateForm,
                                                    template_name='audien/periodoletivo_update.html'), 
                                 name='update_perlet'),
    
    url(r'^mudar_perlet_atual/(?P<pk>\d+)$', views.mudarPeriodoAtual, name='mudarPeriodoAtual'),
    
    url(r'^create_estd/$', CreateView.as_view(model=models.Estudante, 
                                              success_url='/audien/list_estd',
                                              template_name_suffix='_create'), 
                           name='create_estd'),
    
    url(r'^list_estd/$', ListView.as_view(model=models.Estudante, paginate_by=5), name='list_estd'),
    
    url(r'^estd/(?P<pk>\d+)$', UpdateView.as_view(model=models.Estudante,
                                                  template_name='audien/estd_update.html',
                                                  success_url='/audien/list_estd/'
                                                 ), name='update_estd'),
    
    url(r'^del_estd/(?P<pk>\d+)$', DeleteView.as_view(model=models.Estudante,
                                                      success_url='/audien/list_estd/'),
                                   name='delete_estd'),


    url(r'^create_declara/(?P<pk>\d+)$', views.DeclaracaoCreateView.as_view(model=models.Declaracao, 
                                              success_url='/audien/list_estd',
                                              template_name_suffix='_create'), 
                           name='create_declara'),


    url(r'^list_declara/(?P<pk>\d+)$', views.DeclaracaoListView.as_view(model=models.Declaracao), name='list_declara'),

    url(r'^declara/(?P<pk>\d+)$', views.DeclaracaoUpdateView.as_view(model=models.Declaracao,
                                                  template_name='audien/declaracao_update.html'), 
                                  name='update_declara'),

    url(r'^del_declara/(?P<pk>\d+)$', views.DeclaracaoDeleteView.as_view(model=models.Declaracao),
                                      name='delete_declara'),

    
    url(r'^signup/$', views.SignUpView.as_view(), name='signup'),
)
