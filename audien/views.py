# -*- coding: utf-8 -*-
import json
from audien import forms
from django.core.mail import  send_mail
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.views.generic.edit import UpdateView 
from audien.models import Instituicao, PeriodoLetivo, Estudante, Declaracao, UserProfile 
from django.views.generic.list import ListView
from django.views.generic.edit import DeleteView 
from django.core.urlresolvers import reverse
from django.views.generic.edit import UpdateView 
from django.views.generic.edit import CreateView
from django.contrib.messages.views import SuccessMessageMixin
from django import forms
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator


class LoggedInMixin(object):
    
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(LoggedInMixin, self).dispatch(*args, **kwargs)


class PerLetView(LoggedInMixin, ListView):

    
    def get_queryset(self):
        inst = Instituicao.objects.get(pk=self.kwargs['pk'])
        objects = self.model.objects.filter(instituicao=inst).order_by('-pk')
        return objects


class PerLetDeleteView(LoggedInMixin, DeleteView):

    def get_object(self, queryset=None):
        perlet = super(PerLetDeleteView, self).get_object()
        self.success_url = reverse('audien:list_perlet', args=[perlet.instituicao.pk])
        return perlet

class PerLetUpdateView(LoggedInMixin, UpdateView):

    def get_success_url(self):
        
        perlet = super(PerLetUpdateView, self).get_object()
        return reverse('audien:list_perlet', args=[perlet.instituicao.pk])
       
class PerLetCreateView(SuccessMessageMixin, LoggedInMixin, CreateView):

    success_message = "Período Letivo cadastrado com sucesso!"
    
    def form_valid(self, form):
        
        perlet = form.save()
        
        perlet.instituicao.save()
        return super(PerLetCreateView, self).form_valid(form)
        
        

class DeclaracaoCreateView(SuccessMessageMixin, LoggedInMixin, CreateView):

    success_message = 'Declaração para %(nome_estd)s cadastrado com sucesso!'
    
    def get_form(self, form_class):
        
        if self.request.method == 'GET':    
            form = form_class(initial={'estudante': Estudante.objects.get(pk=self.kwargs['pk'])})
            inst = Estudante.objects.get(pk=self.kwargs['pk']).instituicaoAtual
            form.fields['periodo_letivo'].queryset=inst.periodoletivo_set.all()

            form.fields['estudante'].widget = forms.HiddenInput()
            return form
        elif self.request.method == 'POST':
            return super(DeclaracaoCreateView, self).get_form(form_class)

    def get_success_message(self, cleaned_data):
        
        nome_estd = Estudante.objects.get(pk=self.kwargs['pk']).nome
        return self.success_message % dict(cleaned_data,
                                           nome_estd=nome_estd)

class DeclaracaoListView(LoggedInMixin ,ListView):

    paginate_by = 3
    
    def get_queryset(self):
        declaracoes = Estudante.objects.get(pk=self.kwargs['pk']).declaracao_set.all()
        return declaracoes

    def get_context_data(self, **kwargs):
        estd = Estudante.objects.get(pk=self.kwargs['pk'])
        context = super(DeclaracaoListView, self).get_context_data(**kwargs)
        context['nome_estd'] = estd.nome.split(' ')[0] 
        return context

class DeclaracaoUpdateView(LoggedInMixin ,UpdateView):

    def get_form(self, form_class):
            form = super(DeclaracaoUpdateView, self).get_form(form_class)
            if self.request.method == 'GET': 
                form.fields['estudante'].widget = forms.HiddenInput()
                inst = Declaracao.objects.get(pk=self.kwargs['pk']).estudante.instituicaoAtual
                form.fields['periodo_letivo'].queryset=inst.periodoletivo_set.all()
            return form
    
    def get_success_url(self):
        
        estd = Declaracao.objects.get(pk=self.kwargs['pk']).estudante
        return reverse('audien:list_declara', args=[estd.pk]) 

class DeclaracaoDeleteView(LoggedInMixin ,DeleteView):

    def get_success_url(self):
        return reverse('audien:list_declara', args=[self.kwargs['pk']]) 

class SignUpView(SuccessMessageMixin, CreateView):
    model = UserProfile 
    template_name = 'audien/sign_up.html'
        
    success_url = "/audien/"
    success_message = 'Usuário cadastrado com sucesso! Um email foi enviado para sua caixa de email!'

    def form_valid(self, form):
        
        send_mail('Confirmação de cadastro', 
                  'Esse email confirma que vc se cadastrou no site.', 
                  'luheeslo@gmail.com', 
                  [form.instance.email], 
                  fail_silently=True)

        return super(SignUpView, self).form_valid(form)


@login_required
def mudarPeriodoAtual(request, pk):

    inst = PeriodoLetivo.objects.get(pk=pk).instituicao
    pk_perlet = inst.periodo_atual.pk
    inst.mudarPeriodoAtual(pk)
    if request.is_ajax():
        return HttpResponse(pk_perlet) 
    return redirect(reverse('audien:list_perlet', args=[inst.pk]))

